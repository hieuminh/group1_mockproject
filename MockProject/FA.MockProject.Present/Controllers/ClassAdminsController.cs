﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FA.MockProject.Present.Models;
using FA.MockProject.Present.Models.Service;

namespace FA.MockProject.Present.Controllers
{
    [Authorize(Roles ="Manager")]
    public class ClassAdminsController : Controller
    {
        private readonly IClassAdminService _context;

        public ClassAdminsController(IClassAdminService context)
        {
            _context = context;
        }

        // GET: ClassAdmins
        public IActionResult Index()
        {
            return View(_context.GetAll());
        }

        // GET: ClassAdmins/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classAdmin = _context.Details(id);
            if (classAdmin == null)
            {
                return NotFound();
            }

            return View(classAdmin);
        }

        // GET: ClassAdmins/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClassAdmins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("ClassAdminId,Account,PassWord,FullName,DateOfBirth,Gender,PhoneNumber,Email,Status,AuditTrail,IsDeleted")] ClassAdmin classAdmin)
        {
            classAdmin.Status = ClassAdmin.Statuss.New;
            if (ModelState.IsValid)
            {       
                if (_context.Create(classAdmin))
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(classAdmin);
        }

        // GET: ClassAdmins/Edit/5
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classAdmin = _context.Details(id);
            if (classAdmin == null)
            {
                return NotFound();
            }
            return View(classAdmin);
        }

        // POST: ClassAdmins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("ClassAdminId,Account,PassWord,FullName,DateOfBirth,Gender,PhoneNumber,Email,Status,AuditTrail,IsDeleted")] ClassAdmin classAdmin)
        {
            if (id != classAdmin.ClassAdminId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(classAdmin,User.Identity.Name);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassAdminExists(classAdmin.ClassAdminId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(classAdmin);
        }

        // GET: ClassAdmins/Delete/5
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classAdmin = _context.Details(id);
            if (classAdmin == null)
            {
                return NotFound();
            }

            return View(classAdmin);
        }

        // POST: ClassAdmins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            var classAdmin = _context.Details(id);
            if (_context.Delete(classAdmin.ClassAdminId))
            {
                return RedirectToAction(nameof(Index));
            }
            return BadRequest();
        }

        private bool ClassAdminExists(string id)
        {
            return _context.Details(id) != null;
        }
    }
}
