﻿using Microsoft.AspNetCore.Identity;
using System;

namespace FA.MockProject.Present.Models
{
    public class User : IdentityUser
    {
        public bool Gender { get; set; }
        public Statuss Status { get; set; }
        public string AuditTrail { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FullName { get; set; }
        public string Account { get; set; }
        public bool IsDeleted { get; set; }

        public enum Statuss : int
        {
            New=0, Old=1
        }
    }

}
