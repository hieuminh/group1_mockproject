﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FA.MockProject.Present.Models.Repos
{
    public class AdminRepos : IAdminRepos
    {
        public readonly MPConnContext _context;

        public AdminRepos(MPConnContext context)
        {
            _context = context;
        }

        public async Task<int> CreateAsync(User classAdmin)
        {
            _context.Add(classAdmin);
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(User classAdmin)
        {
            _context.Admins.Remove(classAdmin);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Admins.ToListAsync();
        }

        public async Task<User> GetByIdAsync(string id)
        {
            return await _context.Admins.FindAsync(id);
        }
        public async Task<bool> UpdateAsync(User classAdmin)
        {
            _context.Entry(classAdmin).State = EntityState.Modified;
            return await _context.SaveChangesAsync() > 0;
        }
        public IQueryable<User> Get(Expression<Func<User, bool>> filter = null,
            Func<IQueryable<User>, IOrderedQueryable<User>> orderBy = null, string includeProperties = "")
        {
            IQueryable<User> query = _context.Admins;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var item in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(item);
            }

            return orderBy != null ? orderBy(query) : query;
        }

        public async Task<bool> IsExistAsync(string id)
        {
            return await _context.Admins.AnyAsync(c => c.Id == id);
        }
    }
}
