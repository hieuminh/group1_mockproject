﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FA.MockProject.Present.Models.Repos
{
    public interface IAdminRepos
    {
        Task<bool> IsExistAsync(string id);
        Task<IEnumerable<User>> GetAllAsync();

        Task<User> GetByIdAsync(string id);

        Task<int> CreateAsync(User entity);

        Task<bool> UpdateAsync(User entity);

        Task<bool> DeleteAsync(User entity);

        IQueryable<User> Get(Expression<Func<User, bool>> filter = null,
            Func<IQueryable<User>, IOrderedQueryable<User>> orderBy = null,
            string includeProperties = "");
    }
}
