﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FA.MockProject.Present.Models
{
    public class ClassAdmin
    {
        public string ClassAdminId { get; set; }
        public string Account { get; set; }
        public string PassWord { get; set; }
        public string  FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        [Display(Name ="Male")]
        public bool Gender { get; set; }
        public string  PhoneNumber { get; set; }
        public string Email { get; set; }
        public Statuss Status { get; set; }
        public string AuditTrail { get; set; }
        public bool IsDeleted { get; set; }
        public enum Statuss:int
        {
            New,Old
        }
    }

}
