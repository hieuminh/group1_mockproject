﻿using FA.MockProject.Present.Models.Repos;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;



namespace FA.MockProject.Present.Models.Service
{
    public class ClassAdminService : IClassAdminService
    {
        private readonly UserManager<User> _userManager;
        private readonly IAdminRepos _repos;
        public ClassAdminService(UserManager<User> userManager, IAdminRepos adminRepos)
        {
            _userManager = userManager;
            _repos = adminRepos;
        }
        public bool Create(ClassAdmin model)
        {
            var user = ConvertToUSer(model, "CR");
            user.Id = Guid.NewGuid().ToString();
            var result = _userManager.CreateAsync(user, model.PassWord);
            return result.Result == IdentityResult.Success;
        }



        public IEnumerable<ClassAdmin> GetAll()
        {
            List<ClassAdmin> list = new List<ClassAdmin>();
            var result = _repos.GetAllAsync().Result;
            foreach (User user in result)
            {
                list.Add(ConvertToViewModel(user));
            }
            return list;
        }



        public bool Update(ClassAdmin model,string perfomer)
        {
            model.Status = ClassAdmin.Statuss.Old;
            var user = ConvertToUSer(model, "U",perfomer);
            var result = _userManager.UpdateAsync(user);
            return result.Result == IdentityResult.Success;
        }
        public bool Delete(string id)
        {
            var user = _repos.GetByIdAsync(id).Result;
            user.IsDeleted = true;
            var result = _userManager.UpdateAsync(user);
            return result.Result == IdentityResult.Success;
        }
        public ClassAdmin Details(string id)
        {
            var user = _repos.GetByIdAsync(id).Result;
            return ConvertToViewModel(user);
        }
        public User ConvertToUSer(ClassAdmin model, string mode, string perfomer = "Boss")
        {
            //create mode
            if (mode.Equals("CR"))
            {
                return new User()
                {
                    Id = model.ClassAdminId,
                    UserName = model.Account,
                    Account = model.Account,
                    FullName = model.FullName,
                    DateOfBirth = model.DateOfBirth,
                    Gender = model.Gender,
                    PhoneNumber = model.PhoneNumber,
                    Email = model.Email,
                    Status = (User.Statuss)(int)model.Status,
                    AuditTrail = DateTime.Now + " Created by " + perfomer +"\n",
                    IsDeleted = false
                };
            }
            //update mode
            var user = _userManager.FindByIdAsync(model.ClassAdminId).Result;
            user.Id = model.ClassAdminId;
            user.UserName = model.Account;
            user.Account = model.Account;
            user.FullName = model.FullName;
            user.DateOfBirth = model.DateOfBirth;
            user.Gender = model.Gender;
            user.PhoneNumber = model.PhoneNumber;
            user.Email = model.Email;
            user.Status = (User.Statuss)(int)model.Status;
            user.AuditTrail = user.AuditTrail + "\n" + DateTime.Now + " Update by " + perfomer;
            user.IsDeleted = model.IsDeleted;
            return user;



        }
        public ClassAdmin ConvertToViewModel(User model)
        {
            ClassAdmin user = new ClassAdmin()
            {
                ClassAdminId = model.Id,
                Account = model.UserName,
                FullName = model.FullName,
                DateOfBirth = model.DateOfBirth,
                Gender = model.Gender,
                PhoneNumber = model.PhoneNumber,
                Email = model.Email,
                Status = (ClassAdmin.Statuss)(int)model.Status,
                AuditTrail = model.AuditTrail,
                IsDeleted = model.IsDeleted
            };
            return user;
        }
    }
}