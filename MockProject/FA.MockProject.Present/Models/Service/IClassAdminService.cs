﻿using System.Collections.Generic;



namespace FA.MockProject.Present.Models.Service
{
    public interface IClassAdminService
    {
        public IEnumerable<ClassAdmin> GetAll();
        public bool Create(ClassAdmin model);
        public bool Update(ClassAdmin model,string perfomer);
        public bool Delete(string id);
        public ClassAdmin Details(string id);
        public User ConvertToUSer(ClassAdmin model, string mode, string perfomer);
        public ClassAdmin ConvertToViewModel(User model);
    }
}




