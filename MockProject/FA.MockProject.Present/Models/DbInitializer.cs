﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.MockProject.Present.Models
{
    public class DbInitializer
    {
        public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
           
        }
        private static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByEmailAsync("admin@com").Result == null)
            {
                var user = new User()
                {
                    UserName = "admin@com",
                    Email = "admin@com"
                };

                IdentityResult result = userManager.CreateAsync(user, "Admin@com1").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Manager").Wait();
                }
            }
            if (userManager.FindByEmailAsync("user@com").Result == null)
            {
                var user = new User()
                {
                 
                    UserName = "user@com",
                    Email = "user@com"
                };

                IdentityResult result = userManager.CreateAsync(user, "User@com1").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "User").Wait();
                }
            }

        }
        private static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Manager").Result)
            {
                var result = roleManager.CreateAsync(new IdentityRole() { Name = "Manager" }).Result;
            }

            if (!roleManager.RoleExistsAsync("User").Result)
            {
                var result = roleManager.CreateAsync(new IdentityRole() { Name = "User" }).Result;
            }
        }
    }
}

